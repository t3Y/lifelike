# LifeLike 

A HTML5+JS implementation of Life-Like cellular automata.

You can find a preview [here](https://t3Y.eu/preview/lifelike/).

## Usage

Tweak the settings to your preference, bring some initial cells to life with the draw tool and click the _start_ button. For more details have a look at _help.html_.

## Installation

To host your own version of LifeLike, simply clone the repository and host the folder somewhere. You can also use it by opening *index.html* with a webbrowser, but certain links and functions will need some tweaking in order to point to *index.html* rather than *"./"*.

## TODO: 
* Extend the help page
* Add a more varied selection of brushes
* Make custom brushes possible? 
