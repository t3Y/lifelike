<!DOCTYPE HTML>
<!--
    LifeLike
    Copyright (C) 2019 contact@t3Y.eu

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="style/style.css">
        <link rel="icon" href="img/favicon.png">
        <title>Lifelike Cellular Automata</title>
    </head>
    <body>
        <div class="longtext">
        <h1 id="lifelike"><a href="help.html">LifeLike - Help</a></h1>
        <p>This is the Help page for LifeLike. It contains a <a href="#usage">usage guide</a> as well as some general information regarding <a href="#gamerules">Game Rules</a>.</p>
        <p>Click <a href="./">here</a> to go back to the main page.</p>
        <h2 id="usage"><a href="#usage">General Usage</a></h2>
        <p>The page is split into two parts:</p>
        <p><strong>The World canvas</strong> contains the current state of the world. When the simulation is running the canvas changes in accordance to the chosen Game Rules with the sole exception of border cells, which remain unchanged in each iteration. The generations counter keeps track of the number of simulated generations.</p>
        <p><strong>The ToolBox</strong> provides tools to manipulate the Game World.</p>
        <p>The Settings Tool</p>
        <ul>
        <li><i>World size</i> lets you specify the width and height of the world, the unit being cells.</li>
        <li><i>Cell size</i> is the size, in pixels, of a single cells.</li>
        <li><i>Spacing</i> is used to change the space, in pixels, between individual cells. </li>
        <li>The <i>Game Rule</i> is the rule of the simulated World: A cell is born when <i>B</i> of its neighbors are alive, survives when <i>S</i> of its neighbors are alive, and dies otherwise. The values of <i>B</i> and <i>S</i> are given as a concatenation of possible values. For instance <i>S23</i> means a cell survives if it has 2 or 3 neighbors. Invalid values are ignored. See also: <a href="#gamerules">Interesting Game Rules</a></li>
        <li><i>Colors</i> lets you change the color of live and dead cells respectively. Dead cells are cells that have been alive previously but currently are not.</li>
        <li>The <i>Save world state</i> checkbox allows to save the current state of the Game world. This can be used to share configurations by sharing the generated URL.</li>
        <li>The <i>submit</i> button loads a new page with the specified settings.</li>
        </ul>
        <p>The Draw Tool</p>
        <ul>
        <li>The brush input is a list of brushes that can be used to paint on the world canvas. They are ordered by Game Rule.</li>
        <li>The <i>clear</i> and <i>create</i> buttons let you switch between giving life to cells and resetting cells to their default, inactive state.</li>
        </ul>
        <p>The Controls </p>
        <ul>
        <li><i>generations</i> lets you specify how many generations are to be simulated once the start button is pressed. The default value of <i>-1</i> causes the simulation to go on indefinitely unless it is manually interrupted.</li>
        <li><i>idle time</i> is the time, in milliseconds, between each simulation of a generation. A higher value will result in a slower evolution of the Game World.</li>
        <li>When the <i>start</i> button is clicked, the specified number of generations are simulated.</li>
        <li>During a simulation the <i>stop</i> button can be used to interrupt and the simulation.</li>
        <li>The <i>reset</i> button can be used at any moment to reset the world to the state it was in when the page was loaded.</li>
        </ul>
        <h2 id="gamerules"><a href="#gamerules">Interesting Game Rules</a></h2>
        <p>At every step of the simulation, a specific cell's state is calculated using the Game Rules. The default Rules are those of the Game of Life, <i>B3/S23</i>:
        </p>
        <ul>
            <li>
                A cell is born if <i>3</i> of its neighbors are alive
            </li>
            <li>
                A cell survives if <i>2</i> or <i>3</i> of its neighbors are alive
            </li>
            <li>
                Otherwise, a cell dies
            </li>
        </ul>
        <p>
        The Game of Life's rules are the most noteworthy as they result in an incredibly complex and rich world. However other Rules can also be interesting.
        </p>
        <table class="defTable">
            <caption>
                Notable Life-like rules - Taken from <a href="https://en.wikipedia.org/wiki/Life-like_cellular_automaton#A_selection_of_Life-like_rules">Wikipedia</a>
            </caption>
            <tbody>
                <tr>
                    <th>Rule</th>
                    <th>Name</th>
                    <th>Description</th>
                </tr>
                <tr>
                    <td><a href="./?settings=true&survival=1357&birth=1357">B1357/S1357</a></td>
                    <td>Replicator</td>
                    <td>Edward Fredkin's replicating automaton: every pattern is eventually replaced by multiple copies of itself.</td>
                </tr>
                <tr>
                    <td><a href="./?settings=true&survival=&birth=2">B2/S</a></td>
                    <td>Seeds</td>
                    <td>All patterns are phoenixes, meaning that every live cell immediately dies, and many patterns lead to explosive chaotic growth. However, some engineered patterns with complex behavior are known.</td>
                </tr>
                <tr>
                    <td><a href="./?settings=true&survival=4&birth=25">B25/S4</a></td>
                    <td></td>
                    <td>This rule supports a small self-replicating pattern which, when combined with a small glider pattern, causes the glider to bounce back and forth in a pseudorandom walk.</td>
                </tr>
                <tr>
                    <td><a href="./?settings=true&survival=0123456789&birth=3">B3/S012345678</a></td>
                    <td>Life without Death</td>
                    <td>Also known as Inkspot or Flakes. Cells that become alive never die. It combines chaotic growth with more structured ladder-like patterns that can be used to simulate arbitrary Boolean circuits.</td>
                </tr>
                <tr>
                    <td><a href="./">B3/S23</a></td>
                    <td>Life</td>
                    <td>Highly complex behavior.</td>
                </tr>
                <tr>
                    <td><a href="./?settings=true&survival=34&birth=34">B34/S34</a></td>
                    <td>34 Life</td>
                    <td>Was initially thought to be a stable alternative to Life, until computer simulation found that larger patterns tend to explode. Has many small oscillators and spaceships.</td>
                </tr>
                <tr>
                    <td><a href="./?settings=true&survival=5678&birth=35678">B35678/S5678</a></td>
                    <td>Diamoeba</td>
                    <td>Forms large diamonds with chaotically fluctuating boundaries. First studied by Dean Hickerson, who in 1993 offered a $50 prize to find a pattern that fills space with live cells; the prize was won in 1999 by David Bell.</td>
                </tr>
                <tr>
                    <td><a href="./?settings=true&survival=125&birth=36">B36/S125</a></td>
                    <td>2x2</td>
                    <td>If a pattern is composed of 2x2 blocks, it will continue to evolve in the same form; grouping these blocks into larger powers of two leads to the same behavior, but slower. Has complex oscillators of high periods as well as a small glider.</td>
                </tr>
                <tr>
                    <td><a href="./?settings=true&survival=23&birth=36">B36/S23</a></td>
                    <td>HighLife</td>
                    <td>Similar to Life but with a small self-replicating pattern.</td>
                </tr>
                <tr>
                    <td><a href="./?settings=true&survival=34678&birth=3678">B3678/S34678</a></td>
                    <td>Day &amp; Night</td>
                    <td>Symmetric under on-off reversal. Has engineered patterns with highly complex behavior.</td>
                </tr>
                <tr>
                    <td><a href="./?settings=true&survival=245&birth=368">B368/S245</a></td>
                    <td>Morley</td>
                    <td>Named after Stephen Morley; also called Move. Supports very high-period and slow spaceships.</td>
                </tr>
                <tr>
                    <td><a href="./?settings=true&survival=35678&birth=4678">B4678/S35678</a></td>
                    <td>Anneal</td>
                    <td>Also called the twisted majority rule. Symmetric under on-off reversal. Approximates the curve-shortening flow on the boundaries between live and dead cells.</td>
                </tr>
            </tbody>
        </table>
        </div>
    </body>
</html>
