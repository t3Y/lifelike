/*
 *  this file is part of LifeLike
 *  Copyright (C) 2019 contact@t3Y.eu
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * This file contains all methods necessary for the draw tool
 * The draw tool allows the user to bring cells to life or to reset them
 * by clicking them
 */

var brushes = {
    single_cell: [[0,0]],
    //common b2/s23 still lives
    block: [[0,0],[0,1],[1,0],[1,1]],
    beehive: [[1,0],[2,0],[0,1],[3,1],[1,2],[2,2]],
    loaf: [[1,0],[2,0],[0,1],[3,1],[1,2],[3,2],[2,3]],
    boat: [[0,0],[1,0],[0,1],[2,1],[1,2]],
    tub: [[1,0],[0,1],[2,1],[1,2]],
    //common b2/s23 oscillators
    blinker: [[0,0],[1,0],[2,0]],
    toad: [[1,0],[2,0],[3,0],[0,1],[1,1],[2,1]],
    beacon: [[0,0],[1,0],[0,1],[3,2],[2,3],[3,3]],
    pentadecathlon: [[2,0],[7,0],[0,1],[1,1],[3,1],[4,1],[5,1],[6,1],[8,1],[9,1],[2,2],[7,2]],
    pulsar: [[2,0],[3,0],[4,0],[8,0],[9,0],[10,0],[0,2],[5,2],[7,2],[12,2],[0,3],[5,3],[7,3],[12,3],[0,4],[5,4],[7,4],[12,4],[2,5],[3,5],[4,5],[8,5],[9,5],[10,5],[2,7],[3,7],[4,7],[8,7],[9,7],[10,7],[0,8],[5,8],[7,8],[12,8],[0,9],[5,9],[7,9],[12,9],[0,10],[5,10],[7,10],[12,10],[2,12],[3,12],[4,12],[8,12],[9,12],[10,12]],
    //common b2/s23 spaceships
    glider: [[0,0],[1,0],[2,0],[0,1],[1,2]],
    lightweight_spaceship: [[1,0],[4,0],[0,1],[0,2],[4,2],[0,3],[1,3],[2,3],[3,3]],
    middleweight_spaceship: [[3,0],[1,1],[5,1],[0,2],[0,3],[5,3],[0,4],[1,4],[2,4],[3,4],[4,4]],
    heavyweight_spaceship: [[3,0],[4,0],[1,1],[6,1],[0,2],[0,3],[6,3],[0,4],[1,4],[2,4],[3,4],[4,4],[5,4]],
    //other b2/s23 configurations of interest
    r_pentomino: [[1,0],[2,0],[0,1],[1,1],[1,2]],
    diehard: [[6,0],[0,1],[1,1],[1,2],[5,2],[6,2],[7,2]],
    acorn: [[1,0],[3,1],[0,2],[1,2],[4,2],[5,2],[6,2]],
    gosper_glider_gun: [[24,0],[22,1],[24,1],[12,2],[13,2],[20,2],[21,2],[34,2],[35,2],[11,3],[15,3],[20,3],[21,3],[34,3],[35,3],[0,4],[1,4],[10,4],[16,4],[20,4],[21,4],[0,5],[1,5],[10,5],[14,5],[16,5],[17,5],[22,5],[24,5],[10,6],[16,6],[24,6],[11,7],[15,7],[12,8],[13,8]],
    centinal: [[0,0],[1,0],[50,0],[51,0],[1,1],[50,1],[1,2],[3,2],[25,2],[26,2],[48,2],[50,2],[2,3],[3,3],[12,3],[25,3],[26,3],[39,3],[40,3],[48,3],[49,3],[11,4],[12,4],[39,4],[41,4],[10,5],[11,5],[41,5],[11,6],[12,6],[15,6],[16,6],[39,6],[40,6],[41,6],[11,10],[12,10],[15,10],[16,10],[39,10],[40,10],[41,10],[10,11],[11,11],[41,11],[11,12],[12,12],[39,12],[41,12],[2,13],[3,13],[12,13],[25,13],[26,13],[39,13],[40,13],[48,13],[49,13],[1,14],[3,14],[25,14],[26,14],[48,14],[50,14],[1,15],[50,15],[0,16],[1,16],[50,16],[51,16]],
    puffer_train: [[1,0],[2,0],[3,0],[15,0],[16,0],[17,0],[0,1],[3,1],[14,1],[17,1],[3,2],[8,2],[9,2],[10,2],[17,2],[3,3],[8,3],[11,3],[17,3],[2,4],[7,4],[16,4]],
    //b1/s12
    quadruple_sierpinski_triangle: [[0,0]]
};


var mode = "create";
var brush = "single_cell";
var freeDrawing = false;
function toggleDrawMode(button) {

    let enabledButtons = document.getElementsByClassName("selectedButton");
    enabledButtons[0].className = "otherButton";
    button.className = "selectedButton";
    if (button.id === "createButton") {
        mode = "create";
    } else if(button.id === "resetButton") {
        mode = "reset";
    }

}

/*
 * The following listeners allow drawing freely on the canvas
 * change keeps track of brush changes
 * mousedown enables the freeDrawing and enables the clicked cell
 * if freeDrawing is active, mousemove changes the state of cells under the cursor according 
 * to the current mode
 * mouseup disables the freeDrawing
 */
function addDrawingListeners() {
    document.getElementById("brush").addEventListener("change", function(e) {
        brush = e.target.value.toLowerCase().split(" (")[0].replace(/ /g, "_");
    }, false);
    document.getElementById("canvas").addEventListener("mousemove", function(e) {
        if (freeDrawing) {
            modifyAtCursor(e);
        }
    }, false);

    document.getElementById("canvas").addEventListener("mousedown", function(e) {
        modifyAtCursor(e);
        freeDrawing = true;
    }, false);

    document.getElementsByTagName("html")[0].addEventListener("mouseup", function(e) {
        freeDrawing = false;
    }, false);
}

/*
 * Modify the cell state at the cursor's position
 */
function modifyAtCursor(e) {
    //get the correct cursor position (canvas coordinates)
    let rect = world.canvas.getBoundingClientRect();
    let x = e.clientX - rect.left;
    let y = e.clientY - rect.top;

    //convert to world coordinates
    x = Math.floor(x/(world.cellSize + world.spacing));
    y = Math.floor(y/(world.cellSize + world.spacing));

    //apply brush - if out of bound cells are affected catch() the error
    brushes[brush].forEach(function(c){
        try {
            //change cell state
            if (mode === "create") {
                world.createCell(x + c[0], y + c[1]);
            } else if (mode === "reset") {
                world.resetCell(x + c[0], y + c[1]);
            }
        } catch (e){}
    });
}
