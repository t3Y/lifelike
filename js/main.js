/*
 *  this file is part of LifeLike
 *  Copyright (C) 2019 contact@t3Y.eu
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


world.init(loadSettings());
addDrawingListeners();

function start() {
    document.getElementById("start").className = "disabled";
    document.getElementById("stop").className = "enabled";
    let gens = document.getElementById("gens").value;
    let idle = document.getElementById("idle").value;
    world.start(gens, idle);

}

function stop() {
    document.getElementById("stop").className = "disabled";
    document.getElementById("start").className = "enabled";
    world.stop();
}

function reset() {
    world.stop();
    world.reset();
}


/*
 * Utility functions
 */
colorParent(document.getElementsByName("aliveColor")[0]);
colorParent(document.getElementsByName("deadColor")[0]);
function colorParent(e) {
    e.parentElement.style.backgroundColor = e.value;
}
