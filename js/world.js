/*
 *  this file is part of LifeLike
 *  Copyright (C) 2019 contact@t3Y.eu
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * This file contains the world object and its methods
 */
var world = {

    canvas: document.getElementById("canvas"),
    context: this.canvas.getContext("2d"),
    steps: 0,
    running: false,
    counter: document.getElementById("counter"),

    init: function(settings) {
        //load settings
        this.width = settings.width;
        this.height = settings.height;
        this.cellSize = Math.max(1,parseInt(settings.cellSize));
        this.spacing = Math.max(0,parseInt(settings.spacing));
        this.birthArr = settings.birthArr;
        this.survivalArr = settings.survivalArr;
        this.initialStateRaw = settings.initialState;
        this.initialState = {};
        //check game rules for validity
        let bRule = settings.birth.toString().split("");
        let bArr = [];
        bRule.forEach(function(value) {
            value = parseInt(value);
            if (value != 9) bArr.push(value);
        });
        if (bArr.length > 0) this.birthArr = bArr; 

        let sRule = settings.survival.toString().split("");
        let sArr = [];
        sRule.forEach(function(value) {
            value = parseInt(value);
            if (value != 9) sArr.push(value);
        });
        if (sArr.length > 0) this.survivalArr = sArr; 
        //load colors
        this.backgroundColor = settings.backgroundColor;
        this.deadColor = settings.deadColor;
        this.aliveColor = settings.aliveColor;
        //adjust canvas size
        this.canvas.height = this.height * (this.cellSize + this.spacing);
        this.canvas.width = this.width * (this.cellSize + this.spacing);
        //set default execution settings
        this.targetStep = settings.generations;
        this.idle = settings.idle;
        //initialise worldState arrays
        this.worldState0 = new Array(this.width);
        for (let i = this.width-1; i >= 0; i--) {
            this.worldState0[i] = new Array(this.height);
        }
        //terrible hack to duplicate the two-dimensional array
        this.worldState1 = JSON.parse(JSON.stringify(this.worldState0));
        this.history = JSON.parse(JSON.stringify(this.worldState0));
        this.reset();
    },


    /*
     * Empty the world's canvas and reset all cells
     */
    reset: function() {
        this.steps = 0;
        this.counter.innerHTML = "0";
        let initCells = [];
        if (this.initialStateRaw != "blank") {
            this.initialState = JSON.parse(this.initialStateRaw.replace(/%22/g, '"').replace(/%7B/g, "{").replace(/%7D/g, "}") );
            this.initialStateRaw = "blank";
        }
        for (let i=0; i<this.width; i++) {
            if (this.initialState[i] != undefined) {
                initCells = this.initialState[i].split("-");
            } else {
                initCells = [];
            }
            for (let j=0; j<this.height; j++) {
                this.resetCell(i,j);
                if (initCells.length > 0 && initCells.indexOf(j.toString()) != -1) {
                    this.createCell(i,j);
                }
            }
        }
    },

    /*
     * Set the idle time between each simulation of the world
     */
    setIdleLength: function(val) {
        val = parseInt(val);
        if (val > -1) {
            this.idle = val;
        } else {
            this.idle = 500;
        }
    },

    /*
     * Set the target step of the world. If 0 or less is provided there is no limit
     */
    setTargetStep: function(val) {
        val = parseInt(val);
        if (val > 0) {
            this.targetStep = this.steps + val;
        } else {
            this.targetStep = -1;
        }
    },

    stop: function() {
        this.running = false;
    },
    /*
     * Starts and runs the simulation until it is manually stopped or a preset-limit
     * is reached.
     */
    start: function(gens, idle) {
        if (!this.running) {
            this.setTargetStep(gens);
            this.setIdleLength(idle);
            this.running = true;
            this.run();
        }
    },

    /*
     * Calculate the next iteration of the world
     */
    run: function() {
        if (this.running && (this.targetStep < 0 || this.steps < this.targetStep)) {
            this.steps++;
            let oldState = this.worldState0;
            let newState = this.worldState1;
            if (this.steps % 2 == 0) {
                oldState = this.worldState1;
                newState = this.worldState0;
            }
            for (let i = this.width-2; i>0; i--) {
                for (let j = this.height-2; j>0; j--) {
                    let liveNeighbors = oldState[i-1][j] + oldState[i+1][j] + oldState[i][j-1] + oldState[i][j+1] + oldState[i-1][j-1] + oldState[i-1][j+1] + oldState[i+1][j-1] + oldState[i+1][j+1]; 
                    if (!oldState[i][j] && this.birthArr.indexOf(liveNeighbors) != -1) {
                        newState[i][j] = 1;
                        this.history[i][j] = 1;
                        this.colorCell(i, j, this.aliveColor);
                    } else if (oldState[i][j] && this.survivalArr.indexOf(liveNeighbors) != -1) {
                        newState[i][j] = 1;
                        this.history[i][j] = 1;
                        this.colorCell(i, j, this.aliveColor);
                    } else {
                        newState[i][j] = 0;
                        if (this.history[i][j] == 1) {
                            this.colorCell(i, j, this.deadColor);
                        } else {
                            this.colorCell(i, j, this.backgroundColor);
                        }
                    }
                }
            }
            this.counter.innerHTML = this.steps;
            setTimeout(function(){world.run();}, world.idle);
        } else {
            //calls the main stop function, resets buttons etc..
            stop();
        }

    },

    /*
     * Set cell at (x,y) to alive and color the cell on the canvas
     */
    createCell: function(x, y) {
        this.worldState0[x][y] = 1;
        this.worldState1[x][y] = 1;
        this.history[x][y] = 1;
        this.colorCell(x, y, this.aliveColor);
    },

    /*
     * Set cell at (x,y) to dead and color the cell on the canvas to background color
     */
    resetCell: function(x, y) {
        this.worldState0[x][y] = 0;
        this.worldState1[x][y] = 0;
        this.history[x][y] = 0;
        this.colorCell(x, y, this.backgroundColor);
    },
    /*
     * Method to color a specific cell. The color itself has to be passed
     * the x and y coordinates are cell coordinates, not canvas coordinates!
     */
    colorCell: function(x, y, color) {
        this.context.fillStyle = color;
        this.context.fillRect(x*(this.cellSize + this.spacing), y*(this.cellSize + this.spacing), this.cellSize, this.cellSize);
    }

};
