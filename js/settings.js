/*
 *  this file is part of LifeLike
 *  Copyright (C) 2019 contact@t3Y.eu
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * This file contains all methods necessary for loading settings
 * on page load and saving settings
 */

/*
 * Default settings, if something is wrong with the submitted settings, 
 * or if none are submitted, these values are used
 */
var uSettings = {
    width: 100,
    height: 50,
    cellSize: 10,
    spacing: 1,
    birthArr: [],
    survivalArr: [],
    birth: 3,
    survival: 23,
    generations: -1,
    idle: 500,
    backgroundColor: "#bfbfbf",
    deadColor: "#bfbfbf",
    aliveColor: "#000000",
    initialState: "blank"
};

/*
 * Load the settings given by the URL parser. No verification for the values, if there
 * is a key:value pair, it is included in the settings, no matter what
 */
function loadSettings() {
    let newSettings = parseURL(location.href);
    if (newSettings != []) {
        newSettings.forEach(function(pair) {
            try {
                let key = pair[0];
                let value = pair[1];
                uSettings[key] = value;
            } catch (e) {
                console.log(e.toString());
            }
        });
        
    }
    displaySettings();
    return uSettings;
}

/*
 * Try to put the URL into key:value pairs
 */
function parseURL(url) {
    if (url.indexOf("?settings=true&") === -1) {
        return [];
    } else {
        let parameterString = url.split("?settings=true&")[1];
        let params = parameterString.split("&");
        let output = [];
        for (let i=0; i<params.length; i++) {
            output.push(params[i].split("="));
        }
        return output;
    }
}

/*
 * Fill the setting inputs on the page with the current setting value
 */
function displaySettings() {
    let pageSettings = document.getElementsByClassName("setting");
    for (let i = pageSettings.length-1; i >= 0; i--) {
        pageSettings[i].value = uSettings[pageSettings[i].name];
    }
}

/*
 * this function is called when the user changes the settings
 * The settings are pushed into the URL as "get-attributes" and the location set 
 * to this new URL.
 * However since the settings contain the '#' character, location.assign won't
 * actually load the page, we need to force reload the page.
 * There must be a better solution but for now this works.
 * No validation at this stage, data could be trash
 */
function saveSettings() {
    let newSettings = document.getElementsByClassName("setting");
    let url = "?settings=true";
    for (let i = newSettings.length-1; i >= 0; i--) {
        url += "&" + newSettings[i].name + "=" + newSettings[i].value;
    }
    let saveWorldState = document.getElementById("saveWorldState");
    if (saveWorldState.checked) {
        let liveCells = {};
        let state = world.worldState1;
        if (world.steps % 2 == 0) {
            state = world.worldState0;
        }
        for (let i=0; i<world.width; i++) {
            for (let j=0; j<world.height; j++) {
                if (state[i][j] === 1) {
                    if (liveCells[i] === undefined) {
                        liveCells[i] = j.toString();
                    } else {
                        liveCells[i] += "-" + j.toString();
                    }
                }
            }
        }
        liveCells = JSON.stringify(liveCells);
        //keep the max url length to something sane
        if (state.length < 1000) {
            url += "&initialState=" + liveCells;
        }

    }
    location.assign("./" + url);
    //TODO: change page load (the page get cached(?) in some browsers)
    setTimeout(function(){location.reload(true);}, 200);
}
